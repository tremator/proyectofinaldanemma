
#------------------------------------------------imports necesarios----------------------------------------------------#
import random as rd
import sys as sy

#----------------------------------------------listas y matrices usadas------------------------------------------------#
terrestres=["avionies","carro","moto","barco"]
extraterrestres=["naveNodrisa","naveDeAtaque","naveDeApoyo"]

matriz1=[]
matriz2=[]

#----------------------------------------Creacion clases y constructores-----------------------------------------------#
class Vehiculo:
    def __init__(self,tipoObjeto,municiones,armas):
        self.tipoObjeto=tipoObjeto
        self.municiones=municiones
        self.armas=armas

class Jugador:
    def __init__(self,pnombre):
        self.nombre=pnombre

    def disparar(self,pfila,pcolumna,pmatriz):
        listatipoObjeto=list()
        for b in pmatriz:
            arm=(([str(y.tipoObjeto) for y in b]))
            listatipoObjeto.append(arm)
        #print(listatipoObjeto)
        if listatipoObjeto[pfila][pcolumna] == "0":
            print("has fallado")
        else:
            print("has destruido un Vehiculo de tipo: ",listatipoObjeto[pfila][pcolumna])
            nulo=Vehiculo("0",0,0)
            pmatriz[pfila][pcolumna]=nulo

#lista=list()
#lista2=list()

#--------------------------------------Contador para naves (Terrestres, Aliens)----------------------------------------#
def contadorNaves(vTerrestre,nAlien,pmatriz1,pmatriz2):
    listatipoObjeto=list()
    contTerrestre = 0
    for b in pmatriz1:
        arm=(([str(y.tipoObjeto) for y in b]))
        listatipoObjeto.extend(arm)
    #print(listatipoObjeto)
    for x in listatipoObjeto:
        if x != "0":
            contTerrestre += 1
    vTerrestre = contTerrestre
    #print("La cantidad de vhehiculos terrestres creados es: ",vTerrestre)

    listatipoObjeto2=list()
    contAliens = 0
    for b in pmatriz2:
        arm=(([str(y.tipoObjeto) for y in b]))
        listatipoObjeto2.extend(arm)
    #print(listatipoObjeto2)
    for x in listatipoObjeto2:
        if x != "0":
            contAliens += 1
    nAlien = contAliens
    #print("La cantidad de naves aliens creadas es: ",nAlien)

    return ("La cantidad de vhehiculos terrestres es: ",vTerrestre," y la cantidad de naves aliens es: ",nAlien)

#--------------------------------------------Crear matrices para escenario de juego------------------------------------#
def crearEscenario(plista,pmatriz):
        for i in range(5):
            pmatriz.append([0] * 5)
        for i in range(5):
            for j in range(5):
                aleatorio = rd.randint(0,2)
                vtipoVehiculo=rd.choice(plista)
                vmuniciones=rd.randint(1,500)
                varmas=rd.randint(1,250)
                objeto=Vehiculo(vtipoVehiculo,vmuniciones,varmas)
                if aleatorio == 1:
                    pmatriz[i][j] = objeto
                else:
                    objeto2=Vehiculo("0",0,0)
                    pmatriz[i][j] = objeto2
        return pmatriz


def estadisticas(matriz1,matriz2):
    # ----------------------------------------CONTADOR Terrestre-------------------------------------------------------#
    vTerrestre=0

    listatipoObjeto=list()
    for b in matriz1:
        arm=(([str(y.tipoObjeto) for y in b]))
        listatipoObjeto.extend(arm)
    #print(listatipoObjeto)
    for x in listatipoObjeto:
        if x != "0":
            vTerrestre += 1

    listamuniciones=list()
    contadorMuniciones1=0

    for b in matriz1:
        mun=(([str(y.municiones) for y in b]))
        listamuniciones.extend(mun)
    #print(listamuniciones)

    for x,y in enumerate(listamuniciones):
        listamuniciones[x]=int(y)
    #print(listamuniciones)
    for s in (listamuniciones):
        contadorMuniciones1=contadorMuniciones1+s
    #print("las municiones son :",contadorMuniciones1)

#-------------------------------------------------CONTADOR Alien-------------------------------------------------------#
    nAlien=0

    listatipoObjeto2=list()
    for b in matriz2:
        arm=(([str(y.tipoObjeto) for y in b]))
        listatipoObjeto2.extend(arm)
    #print(listatipoObjeto2)
    for x in listatipoObjeto2:
        if x != "0":
            nAlien += 1

    #print("la cantidad de vehiculos es de :",nAlien)

    listamuniciones2=list()
    contadorMuniciones2=0

    for b in matriz2:
        municiones=(([str(y.municiones) for y in b]))
        listamuniciones2.extend(municiones)
    #print(listamuniciones2)

    for x,y in enumerate(listamuniciones2):
        listamuniciones2[x]=int(y)
    #print(listamuniciones2)
    for s in (listamuniciones2):
        contadorMuniciones2=contadorMuniciones2+s
    #print("las municiones son :",contadorMuniciones2)

#-------------------------------------------Formula porcentajes--------------------------------------------------------#
    estMun1=0
    estMun2=0
    estTerrestre=0
    estAlien=0
    if contadorMuniciones2 < contadorMuniciones1:
        estMun1 = int(((contadorMuniciones1 - contadorMuniciones2)/contadorMuniciones2)*100)
        '''print(contadorMuniciones1,"/",contadorMuniciones2,"estmun")
        print(estMun1,"estmun")'''
    else:
        estMun2 = int(((contadorMuniciones2 - contadorMuniciones1)/contadorMuniciones1)*100)

    #------------------------------------------------------------------------------
    if vTerrestre < nAlien:
        estTerrestre = int(((nAlien - vTerrestre)/vTerrestre)*100)

    else:
        estAlien = int(((vTerrestre - nAlien)/nAlien)*100)


    #----------------------------------------------Condicionales-------------------------------------------------------#
    if estTerrestre > 80 or estMun1 > 70:
        print(jugador2.nombre," a perdido", jugador1.nombre," es el ganador.")
        sy.exit()
    elif estAlien > 80 or estMun2 > 70:
        print(jugador1.nombre," a perdido", jugador2.nombre," es el ganador.")
        sy.exit()
    else:
        return (estAlien,estMun1,estMun2,estTerrestre)

#----------------------------------------------------------------Asignacion nombres------------------------------------#
jugador=1
while jugador<=2:
    nombre=input("Ingrese Nombre del Jugador : ")
    if jugador==1:
        jugador1=Jugador(nombre)
    else:
        jugador2=Jugador(nombre)
    jugador=jugador+1
#---------------------------------------------Crear Escenarios(Matriz)-------------------------------------------------#
crearEscenario(terrestres,matriz1)
crearEscenario(extraterrestres,matriz2)

#----------------------------------------Inicio Juego con caracteristicas añadidas-------------------------------------#
print("---------Bienvenidos al Muego Guerra de Mundos------------")
nAlien=0
vTerrestre=0
cont = contadorNaves(vTerrestre,nAlien,matriz1,matriz2)

opcion=0
turno=0
while opcion !=7 :
    if turno==0:
        estadisticas(matriz1,matriz2)
        print(jugador1.nombre)
        retirarse=input("desea retirarse? (s/n)")
        if retirarse.lower()=="s":
            cont2=contadorNaves(vTerrestre,nAlien,matriz1,matriz2)
            print("Generados : ",cont)
            print("Actuales: ",cont2)
           # print(cont2)
            print("el jugador",jugador2.nombre,"ha ganado")
            break
        else:
            estadisticas(matriz1,matriz2)
            pfila=int(input("ingrese la fila en la cual desea disparar: "))
            pcolumna=int(input("ingrese la columna en la cual desea disparar: "))
            jugador1.disparar(pfila,pcolumna,matriz2)
            estadisticas(matriz1,matriz2)

        turno+=1
    else:
        print(jugador2.nombre)
        retirarse=input("desea retirarse? (s/n)")
        if retirarse.lower()=="s":
            cont2=contadorNaves(vTerrestre,nAlien,matriz1,matriz2)
            print("la cantidad de vehiculos generados es de : ",cont)
            print("la cantidad de naves actuales es de: ",cont2)
            #print(cont2)
            print("el jugador",jugador1.nombre,"ha hanado")
            break
        else:
            estadisticas(matriz1,matriz2)
            pfila=int(input("ingrese la fila en la cual desea disparar: "))
            pcolumna=int(input("ingrese la columna en la cual desea disparar: "))
            jugador1.disparar(pfila,pcolumna,matriz1)
            turno=0
            estadisticas(matriz1,matriz2)
    opcion +=1
else:
    cont2=contadorNaves(vTerrestre,nAlien,matriz1,matriz2)
    print("la cantidad de vehiculos generados es de : ",cont)
    print("la cantidad de naves actuales es de: ",cont2)

    print("Gracias por jugar a Guerra de Mundos")
